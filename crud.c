#include "include/listaDinamicaCircular.h"
#include <stdio.h>
#include <locale.h>
#define _GERAR_ID_ 0 // Define se o código do cliente será gerado automaticamente
#if _GERAR_ID_ == 1
    #define cadastrarCliente(cabecaLista, cliente) cadastrarCliente(cabecaLista, cliente, 1)
#else
    #define cadastrarCliente(cabecaLista, cliente) cadastrarCliente(cabecaLista, cliente, 0)
#endif
int primeiraExecucao=1, codigoRetornado;
LISTA* listaClientes;

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
    #include <windows.h>
#endif

void limparTela(){
    printf("\033[H\033[2J\033[3J");
}
void clearStdinBuffer(){ // Limpa o buffer de stdIn
    while(getchar() != '\n');
}
int main(){
    CLIENTE cliente = {.id = -1, .nome = "Cliente\0"};
    unsigned int opcaoEscolhida;
    int codigoCliente;

    if(primeiraExecucao){
        #if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
            SetConsoleCP(65001);  // Ativa UTF-8 como CodePage
            SetConsoleOutputCP(65001);  // Ativa UTF-8 como CodePage de saída
            setlocale(LC_ALL, "pt_BR.UTF-8");
        #else
            setlocale(LC_ALL,"Portuguese");
        #endif
        listaClientes = criarLista();
        primeiraExecucao = 0;
    }

    printf(
        "\033[1;32m"
        "======== Menu principal ==========\033[0m\n"
        "1 - Cadastrar cliente \n"
        "2 - Atualizar dados de cliente \n"
        "3 - Buscar cliente \n"
        "4 - Remover cliente \n"
        "5 - Listar clientes \n"
        "0 - Sair \n"
        ">: "
    );
    opcaoEscolhida=13;
    scanf("%1u", &opcaoEscolhida);
    clearStdinBuffer();

    switch(opcaoEscolhida){
        case 1:
            limparTela();
            printf(
                "\033[1;32m"
                "===== Cadastrar cliente =====\033[0m\n"
            );
            #if _GERAR_ID_ == 0
                printf("Código: ");
                scanf("%d", &cliente.id);
                clearStdinBuffer();
                while(cliente.id <= 0){ // Enquanto o código for inválido
                    printf("Código inválido! Tente novamente\nCódigo: ");
                    scanf("%d", &cliente.id);
                    clearStdinBuffer();
                }   
            #endif
            printf("Nome: ");
            scanf(" %[^\n]", cliente.nome);

            codigoRetornado = cadastrarCliente(listaClientes, cliente);

            limparTela();
            if(codigoRetornado == 1){
                printf("Cliente cadastrado com sucesso. \n");
            }else if(codigoRetornado == -1){
                printf("Código de cliente já está cadastrado. \n");
            }else{
                printf("Não foi possível cadastrar o cliente. \n");
            }
            main(); // Volta para main e mostra menu
            break;
        case 2:
            limparTela();
            printf(
                "\033[1;32m"
                "===== Atualizar dados de cliente =====\033[0m\n"
                "Código do cliente: "
            );
            scanf("%d", &codigoCliente);
            clearStdinBuffer();

            codigoRetornado = buscarCliente(listaClientes, codigoCliente, &cliente);
            if(codigoRetornado == 1){
                printf(
                    "\n"
                    "------- Dados salvos -------\n"
                    "Código: %d\n"
                    "Nome: %s\n",
                    cliente.id,
                    cliente.nome
                );
                printf(
                    "------- Novos dados --------\n"
                    "Nome: "
                );
                scanf(" %[^\n]}", cliente.nome);

                codigoRetornado = atualizarCliente(listaClientes, &cliente);

                limparTela();
                if(codigoRetornado == 1){
                    printf("Cadastro do cliente atualizado com sucesso. \n");
                }else{
                    printf("Não foi possível atualizar o cadastro do cliente. \n");
                }
            }else if(codigoRetornado == -1){
                limparTela();
                printf("Não existem clientes cadastrados. \n");
            }else{
                limparTela();
                printf("Não foi possível encontrar o cliente. \n");
            }
            main(); // Volta para main e mostra menu
            break;
        case 3:
            limparTela();
            printf(
                "\033[1;32m"
                "===== Buscar cliente =====\033[0m\n"
                "Código do cliente: "
            );
            scanf("%d", &codigoCliente);
            clearStdinBuffer();

            codigoRetornado = buscarCliente(listaClientes, codigoCliente, &cliente);
            if(codigoRetornado == 1){
                printf(
                    "\n"
                    "------- Dados salvos -------\n"
                    "Código: %d\n"
                    "Nome: %s\n\n"
                    "Pressione enter para continuar... \n",
                    cliente.id,
                    cliente.nome
                );
                while(getchar() != '\n'); // Esperando "Enter" para continuar
                limparTela();
            }else if(codigoRetornado == -1){
                limparTela();
                printf("Não existem clientes cadastrados. \n");
            }else{
                limparTela();
                printf("Não foi possível encontrar o cliente. \n");
            }
            main(); // Volta para main e mostra menu
            break;
        case 4:
            limparTela();
            printf(
                "\033[1;32m"
                "===== Remover cliente =====\033[0m\n"
                "Código do cliente: "
            );
            scanf("%d", &codigoCliente);
            clearStdinBuffer();

            codigoRetornado = removerCliente(listaClientes, codigoCliente);
            limparTela();
            if(codigoRetornado == 1){
                printf("Cliente excluído com sucesso. \n");
            }else if(codigoRetornado == -1){
                limparTela();
                printf("Não existem clientes cadastrados. \n");
            }else{
                printf("Não foi possível remover cliente. \n");
            }
            main(); // Volta para main e mostra menu
            break;
        case 5:
            limparTela();
            printf(
                "\033[1;32m"
                "===== Listar clientes =====\033[0m\n"
            );
            listar_clientes(listaClientes);

            printf("\nPressione enter para continuar... \n");
            while(getchar() != '\n'); // Esperando "Enter" para continuar

            limparTela();
            main(); // Volta para main e mostra menu
            break;
        case 0:
            limparTela();
            printf("Saindo do programa... \n");
            break; // Sai do switch e finaliza o programa
        default:
            limparTela();
            printf("Opção inválida! Escolha novamente. \n");
            main(); // Volta para main e mostra menu
    };
    return 0;
}
