#ifndef _LISTA_DINAMICA_CIRCULAR_H_
#define _LISTA_DINAMICA_CIRCULAR_H_
#define NAME_SIZE 30

/*************** tipos de dados da lista ***********/

struct cliente{ // Define o que é um cliente
    int id;
    char nome[NAME_SIZE];
}typedef CLIENTE;

struct elemento_da_lista{ // Define como é um elemento(nó) desta lista
    CLIENTE dados;
    struct elemento_da_lista *proximo; // Ponteiro para o próximo elemento da lista
    struct elemento_da_lista *anterior; // Ponteiro para o elemento anterior da lista
}typedef ELEM;

typedef ELEM* LISTA; // "Lista" é o um ponteiro para "struct elemento_da_list". Ela será a cabeça/início da lista.

/*************** Funções da lista **************/

// Cria a lista
LISTA* criarLista();

// Verifica se a lista está vazia
int listaEstaVazia(LISTA* cabecaLista);

// Cadastra cliente na lista
int cadastrarCliente(LISTA* cabecaLista, CLIENTE cliente, short int gerarId);

// Atualiza cliente na lista
int atualizarCliente(LISTA* cabecaLista, CLIENTE* cliente);

// Busca e retorna cliente da lista informando o id
int buscarCliente(LISTA* cabecaLista, int id, CLIENTE* clienteDestino);

// Remove cliente da lista informando o id
int removerCliente(LISTA* cabecaLista, int id);

// Lista todos os clientes cadastrados
void listar_clientes(LISTA* cabecaLista);

#endif
