#include "listaDinamicaCircular.h"
#include <stdlib.h>
#include <stdio.h>

/*************** Funções da lista **************/

/* Obs.:
 *   cabecaLista = ponteiro para o primeiro elemento da lista
 *   (*cabecaLista) = é o primeiro elemento da lista
 *
 *        cabecaLista
 *             ↓
 * .. ⇆ [1_elemento = (*cabecaLista)] ⇆ [2_elemento] ⇆ [3_elemento] ⇆ ...
*/

// Cria a lista
LISTA* criarLista(){
    LISTA* cabecaLista = (LISTA*)malloc(sizeof(LISTA));
    if(cabecaLista != NULL){ // Verifica se a lista existe (não é nula)
        *cabecaLista = NULL; // Limpa 1º elemento da lista
    }
    return cabecaLista;
}

// Verifica se a lista está vazia
int listaEstaVazia(LISTA* cabecaLista){
    if(cabecaLista == NULL) return 1; // Verifica se a lista é nula. Uma lista que não existe não pode estar cheia, por isso é retornado 1(verdadeiro)
    if(*cabecaLista == NULL) return 1; // Verifica se o 1º elemento da lista está vazio, é nulo
    return 0;
}

// Cadastra cliente na lista
int cadastrarCliente(LISTA* cabecaLista, CLIENTE cliente, short int gerarId){
    if(cabecaLista == NULL) return 0; // Verifica se a lista é nula (não existe)

    ELEM *noCliente = (ELEM*)malloc(sizeof(ELEM));
    noCliente->dados = cliente;  
    if(gerarId || cliente.id <= 0){ // Verifica se o id deve ser gerado automaticamente
        if(listaEstaVazia(cabecaLista)){
            cliente.id = 1;
        }else{
            cliente.id = (*cabecaLista)->anterior->dados.id +1;
        }
    }
    noCliente->dados.id = cliente.id;

    if(listaEstaVazia(cabecaLista)){ // Não existem elementos na lista. Inserção do 1º elemento
        noCliente->anterior = noCliente;
        noCliente->proximo = noCliente;
        *cabecaLista = noCliente;
    }
    else{ // Inserção no início, meio ou final
        ELEM *noAuxiliar = *cabecaLista;
        while(noAuxiliar->dados.id < cliente.id){ // Percorre lista até que noAuxiliar->dados.id >= cliente.id
            noAuxiliar = noAuxiliar->proximo; // "Incrementa", aponta para o próximo elemento
            if(noAuxiliar == *cabecaLista) break; // Se chegou no começo da lista novamente, encerra
        }
        // Verifica resultado da busca
        if(noAuxiliar->dados.id == cliente.id){
            return -1; // Cliente já cadastrado
        }else{
            // Chegamos no meio da lista: ... noAnterior ⇆ noAuxiliar ⇆ noSeguinte ...
            noCliente->proximo = noAuxiliar;            //... noCliente → noAuxiliar ⇆ noSeguinte ...
            noCliente->anterior = noAuxiliar->anterior; //... noAnterior ← noCliente → noAuxiliar ⇆ noSeguinte ...
            (noCliente->anterior)->proximo = noCliente; //... noAnterior ⇆ noCliente → noAuxiliar ⇆ noSeguinte ...
            noAuxiliar->anterior = noCliente;           //... noAnterior ⇆ noCliente ⇆ noAuxiliar ⇆ noSeguinte ...
            // Verifica se é preciso trocar início da lista
            if(noCliente->dados.id < (*cabecaLista)->dados.id){ // cliente inserido tem id menor?
                *cabecaLista = noCliente; // Então vira o começo da lista
            }
        }
    }
    return 1;
}

// Busca um elemento(nó). Esta é uma função interna
ELEM* buscarElemento(LISTA* cabecaLista, int id){
    // Verifica se a lista é nula (não existe), se está vazia ou id é inválida
    if((cabecaLista == NULL) || listaEstaVazia(cabecaLista) || (id <= 0)) return NULL;

    ELEM *noAuxiliar = *cabecaLista;
    // Buscar o elemento
    while( (noAuxiliar->dados.id < id) && (noAuxiliar->proximo != *cabecaLista) ){ // Para no último elemento ou noAuxiliar->id>= id
        noAuxiliar = noAuxiliar->proximo;
    }
    return noAuxiliar;
}

// Atualiza cliente na lista
int atualizarCliente(LISTA* cabecaLista, CLIENTE* cliente){
    if(listaEstaVazia(cabecaLista)) return -1; // Erro, lista vazia

    ELEM *noAuxiliar = buscarElemento(cabecaLista, cliente->id);
    // Verifica o resultado da busca
    if( (noAuxiliar != NULL) && (noAuxiliar->dados.id == cliente->id)){ // Encontrou
        noAuxiliar->dados = *cliente; // Guarda novos dados do cliente
        return 1;
    }else{ // Não encontrou
        return 0;
    }
}

// Busca e retorna cliente da lista informando o id
int buscarCliente(LISTA* cabecaLista, int id, CLIENTE* clienteDestino){
    if(listaEstaVazia(cabecaLista)) return -1; // Erro, lista vazia

    ELEM *noAuxiliar = buscarElemento(cabecaLista, id);
    // Verifica o resultado da busca
    if( (noAuxiliar != NULL) && (noAuxiliar->dados.id == id)){ // Encontrou
        *clienteDestino = noAuxiliar->dados; // Copia dados do cliente
        return 1;
    }else{ // Não encontrou
        return 0;
    }
}

// Remove cliente da lista informando o id
int removerCliente(LISTA* cabecaLista, int id){
    if(listaEstaVazia(cabecaLista)) return -1; // Erro, lista vazia

    ELEM *noAnterior, *noSeguinte, *noAuxiliar = buscarElemento(cabecaLista, id);
    // Verifica o resultado da busca
    if( (noAuxiliar != NULL) && (noAuxiliar->dados.id == id)){ // Encontrou
        if(noAuxiliar == (noAuxiliar->proximo)){ // Temos apenas 1 elemento na lista?
            *cabecaLista = NULL;
        }else{
            noAnterior = noAuxiliar->anterior;
            noSeguinte = noAuxiliar->proximo;
            /*... noAnterior ⇆ noAuxiliar ⇆ noSeguinte ... */
            noAnterior->proximo = noSeguinte; // ... noAnterior → noSeguinte ...
            noSeguinte->anterior = noAnterior; // ... noAnterior ⇆ noSeguinte ...
            if(noAuxiliar == *cabecaLista){ // Verifica se o elemento que queremos remover é o 1º da lista
                *cabecaLista = noSeguinte; // Muda qual é o início da lista
            }
        }
        free(noAuxiliar); // Libera da memória
        return 1;
    }else{ // Não encontrou
        return 0;
    }
}
// Lista todos os clientes cadastrados
void listar_clientes(LISTA* cabecaLista){
    if(listaEstaVazia(cabecaLista)){
        printf("Não existem clientes cadastrados. \n");
    }else{
        ELEM *noAuxiliar = *cabecaLista;
        printf(
            "\n\033[7m"
            "+------------+-------------------------------+\n"
            "|     ID     |            Cliente            |\n"
            "+------------+-------------------------------+\033[0m\n"
        );
        do{
            printf(
                "| %10d | %-30s|\n"
                "+------------+-------------------------------+\n",
                noAuxiliar->dados.id,
                noAuxiliar->dados.nome
            );
            noAuxiliar = noAuxiliar->proximo;
        }while(noAuxiliar != *cabecaLista);
        
    }
}
