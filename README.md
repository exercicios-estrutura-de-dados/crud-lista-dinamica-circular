# CRUD com lista dinâmica circular

Este é um exercício proposto na disciplina de **Estrutura de Dados 1** do [**Instituto de Informática**](https://inf.ufg.br/) da [**Universidade Federal de Goiás**](https://www.ufg.br/).

Consiste em um programa do tipo CRUD (Create, Read, Update e Delete) usando uma lista dinâmica duplamente encadeada circular.

Cada nó da lista é composto por um valor inteiro que representa o código de um cliente fictício. A chave para busca, atualização ou remoção de um nó é esse código.

Para ativar ou desativar geração de código automaticamente, basta mudar a definição ```_GERAR_ID_``` no arquivo [**crud.c**](./crud.c).
* ```#define _GERAR_ID_ 1``` // geração automática ativada
* ```#define _GERAR_ID_ 0``` // geração automática desativada

## Como compilar e executar?

Os requisitos são:
* [Git](https://git-scm.com/)
* [GNU Make](https://www.gnu.org/software/make/)
* Um Compilador para **C**
    * [GCC](https://gcc.gnu.org/) ou
    * [Clang](https://clang.llvm.org/) ou
    * [MinGW](https://www.mingw-w64.org/)

### Instalação das ferramentas
* No **Windows** utilizando [Chocolatey](https://chocolatey.org/install#individual).

Execute o comando no PowerShell
```powershell
> choco install git mingw make
```

* No **Ubuntu, Debian, Mint, PopOS**...</br>

Execute o comando no Terminal
```bash
$ sudo apt update && sudo apt install git build-essential
```

* No **MacOS** </br>

Instale [Command Line Tools](https://developer.apple.com/download/all/) ou [Xcode](https://developer.apple.com/download/all/)

### Compilando e executando
Basta clonar o repositório e utilizar o comando make para compilar
```bash
$ git clone https://gitlab.com/exercicios-estrutura-de-dados/crud-lista-dinamica-circular.git
$ cd crud-lista-dinamica-circular/
$ make
$ make run
```

## License
This project is licensed under the terms of the [MIT](https://choosealicense.com/licenses/mit/) license.
