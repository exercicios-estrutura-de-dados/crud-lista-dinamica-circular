# Compila o programa inteiro

# Variávies
CC = gcc
CFLAGS = -pedantic-errors -Wall

# Regra : dependências
all: listaDinamicaCircular.o crud.o
	$(CC) $(CFLAGS) crud.o include/listaDinamicaCircular.o -o crud

crud.o: crud.c
	$(CC) $(CFLAGS) -c crud.c -o crud.o

listaDinamicaCircular.o: include/listaDinamicaCircular.h include/listaDinamicaCircular.c
	$(CC) $(CFLAGS) -c include/listaDinamicaCircular.c -o include/listaDinamicaCircular.o

clean:
	rm *.o include/*.o crud

run:
	./crud

debug:
	make CFLAGS+=-g
